using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FishAI
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class FishGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Camera selectedCamera = new Camera();
        Vector2 CenterCoords;
        SpriteFont Font1;
        Vector2 FontPos;

        public FishGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Board.PrepareBoard();
            selectedCamera.Location = new OffsetPosition(0, 0);
            this.Window.Title = "Fish AI";
            this.Window.AllowUserResizing = true;
            Board.GenerateMap(selectedCamera);

            this.IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // use this.Content to load your game content here

            TextureManager.prepareLoading();
            TextureManager.loadTexture(Content.Load<Texture2D>("Fish/SpecularFish"), TextureType.SpecularFish);
            TextureManager.loadTexture(Content.Load<Texture2D>("Fish/TrindentFish"), TextureType.TridentFish);
            TextureManager.loadTexture(Content.Load<Texture2D>("Fish/DebuggingCircle"), TextureType.DebuggingCircle);
            TextureManager.loadTexture(Content.Load<Texture2D>("Fish/Planty"), TextureType.Planty);
            TextureManager.loadTexture(Content.Load<Texture2D>("Fish/PiranhaFish"), TextureType.PiranhaFish);
            TextureManager.loadTexture(Content.Load<Texture2D>("Fish/ScrollingBackground"), TextureType.Background);
            Font1 = Content.Load<SpriteFont>("Font");        
            FontPos = new Vector2(graphics.GraphicsDevice.Viewport.Width / 2,
                graphics.GraphicsDevice.Viewport.Height/6);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            
            foreach (Unit u in Board.Unitlist)
            {
                u.DoTick(gameTime);
            }
            foreach (AIManager a in Board.AIList) {
                a.DoAITick();
            }
            InputState();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            // TODO: Add your drawing code here
            spriteBatch.Begin();
            //spriteBatch.Draw(TextureManager.getTexture(TextureType.SpecularFish), new Vector2(0, 0), Color.White);
            DrawBackdrop();
            DrawEntities();
            if (Options.showDebugCounter) {
                string output = Board.debugCounter.ToString();
                Vector2 FontOrigin = Font1.MeasureString(output) / 2;
                spriteBatch.DrawString(Font1, output, FontPos, Color.Black,
                    0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public void DrawEntities()
        {
            CenterCoords = new Vector2(GraphicsDevice.PresentationParameters.BackBufferWidth / 2, GraphicsDevice.PresentationParameters.BackBufferHeight / 2);
            foreach (Unit entity in Board.Unitlist)
            {
                //BLACK MAGIC:
                Vector2 screencoords = Board.CenteredCam.Location.Vector2 - entity.Location.Vector2 + CenterCoords;
                Texture2D entityTexture = TextureManager.getTexture(entity.Image);
                float heightfactor = (float)entityTexture.Height / entityTexture.Width;
                float scaledsize = (float)(entity.Size * Board.CenteredCam.Zoom);
                Vector2 halfsize = new Vector2(scaledsize, scaledsize* heightfactor);
                Vector2 drawcoords = screencoords - halfsize;
                Vector2 endcoords = screencoords + halfsize;
                Vector2 origin = new Vector2((float)(entity.TextureOffsetX), (float)(entity.TextureOffsetY));
                //Vector2 origin = new Vector2(0.0f, 0.0f);
                
                if (!entity.Mirrored) {
                    spriteBatch.Draw(entityTexture, new Rectangle((int)drawcoords.X, (int)drawcoords.Y, (int)(endcoords.X - drawcoords.X), (int)(endcoords.Y - drawcoords.Y)), null, Color.White, 0, origin,SpriteEffects.None,0);
                }
                else {
                    spriteBatch.Draw(entityTexture, new Rectangle((int)drawcoords.X, (int)drawcoords.Y, (int)(endcoords.X - drawcoords.X), (int)(endcoords.Y - drawcoords.Y)),null, Color.White,0,origin,SpriteEffects.FlipHorizontally,0);
                }
                if (Options.showHitboxes)
                {
                    DebugHitboxes(entity, drawcoords);
                }
            }
        }

        public void DebugHitboxes(Unit entity, Vector2 screencoords)
        {

            spriteBatch.Draw(TextureManager.getTexture(TextureType.DebuggingCircle), new Rectangle((int)((entity.TextureOffsetX * (float)entity.Size) + screencoords.X + (entity.CollisionHB.OffsetVector.X * entity.Size) - entity.CollisionHB.Radius), (int)((entity.TextureOffsetY * (float)entity.Size) + screencoords.Y + (entity.CollisionHB.OffsetVector.Y * entity.Size) - entity.CollisionHB.Radius), (int)(entity.CollisionHB.Radius * 2), (int)(entity.CollisionHB.Radius * 2)), null, new Color(0.8f, 0.6f, 0.0f, 0.3f), 0, new Vector2(0f, 0f), SpriteEffects.None, 0);
            if (entity is Fish) {
                Fish tf = entity as Fish;
                spriteBatch.Draw(TextureManager.getTexture(TextureType.DebuggingCircle), new Rectangle((int)((tf.TextureOffsetX * (float)tf.Size) + screencoords.X + (-tf.BiteHB.OffsetVector.X * entity.Size) - tf.BiteHB.Radius), (int)((tf.TextureOffsetY * (float)tf.Size) + screencoords.Y + (tf.BiteHB.OffsetVector.Y * entity.Size) - tf.BiteHB.Radius), (int)(tf.BiteHB.Radius * 2), (int)(tf.BiteHB.Radius * 2)), null, new Color(0.0f, 0.9f, 0.1f, 0.3f), 0, new Vector2(0f, 0f), SpriteEffects.None, 0);
            }
        }

        public void InputState() {
            KeyboardState state = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();

            
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                Board.RaiseMouseClicked(MouseButton.LeftMB, mouseState, selectedCamera.Location.Vector2 + CenterCoords - new Vector2(mouseState.X, mouseState.Y));
            }


        }

        public void DrawBackdrop()
        {
            int tx = TextureManager.getTexture(TextureType.Background).Width;
            int ty = TextureManager.getTexture(TextureType.Background).Height;
            int x = tx*-2;
            int y = ty*-2;
            int offsetX = (int)selectedCamera.Location.X %tx;
            int offsetY = (int)selectedCamera.Location.Y %ty;
            int maxX=tx*2+GraphicsDevice.PresentationParameters.BackBufferWidth;
            int maxY=ty*2+GraphicsDevice.PresentationParameters.BackBufferHeight;
            while(x < maxX){
                y = ty * -2;
                while (y < maxY) { 
                    spriteBatch.Draw(TextureManager.getTexture(TextureType.Background),new Rectangle(x+offsetX,y+offsetY,tx,ty),Color.White);
                    y += ty;
                }
                x += tx;
            }
        }
    }
}
