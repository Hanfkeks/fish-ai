﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class Hitbox
    {
        public Unit Master;
        private Vector2 _offset;
        private bool mirrorable=false;
        public Vector2 OffsetVector {
            get {
                if (mirrorable&&Master.Mirrored) {
                    return new Vector2(-_offset.X,_offset.Y);
                }
                else return _offset;
            }
            set { _offset = value; }
        }
        public float Radius;

        public bool CollisionCheck(Unit other) {
            if (other != Master)
            {
                return ((other.Location.Vector2+(other.CollisionHB.OffsetVector * (float)other.Size)) - (Master.Location.Vector2 + (OffsetVector * (float)Master.Size))).Length() < other.CollisionHB.Radius + Radius;
                //return ((other.Location.Vector2 + Vector2.Multiply(other.CollisionHB.OffsetVector, (float)other.Size)) - (Master.Location.Vector2 + Vector2.Multiply(OffsetVector, (float)Master.Size))).Length() < other.CollisionHB.Radius + Radius;
            }
            else return false;
            //Yah, i know what you think...
        }

        public Vector2 getUnchangedOffset() {
            return _offset;
        }

        public Hitbox(Unit master, Vector2 offset, float radius, bool mirrorable) {
            this.Master = master;
            this._offset = new Vector2(-offset.X, offset.Y);
            this.Radius = radius;
            this.mirrorable = mirrorable;
        }
    }
}
