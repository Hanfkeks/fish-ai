﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class PiranhaFish:Fish
    {
        public PiranhaFish(Vector2 v)
            : base(v)
        {
            TextureOffsetX = 0.4f;
            TextureOffsetY = 0.5f;
            this.Image = TextureType.PiranhaFish;
            friction = 0.0025f;
            FactorCollisionHB = 0.7f;
            FactorBiteHB = 0.3f;
            edible.Add(FoodType.Meat);
            Spawn();
            //AI Test
            switch (Options.PfishAI) { 
                case PiranhaFishAIMode.Hunter:
                    FishCM = new HunterAI(this);
                    Board.AIList.AddFirst((AIManager)FishCM);
                    break;
                case PiranhaFishAIMode.Survival:
                    FishCM = new SurvivalAI(this);
                    Board.AIList.AddFirst((AIManager)FishCM);
                    break;
                case PiranhaFishAIMode.Fuzzy:
                    FishCM = new FuzzyAI(this);
                    Board.AIList.AddFirst((AIManager)FishCM);
                    break;
                default:
                    break;
            }
            
        }

        public void Spawn() {

            CollisionHB = new Hitbox(this, new Vector2(0f, 0f), (float)(Size * FactorCollisionHB), false);
            BiteHB = new Hitbox(this, new Vector2(0.6f, 0.0f), (float)(Size * FactorBiteHB), true);
            Size = ((Board.Maprand.NextDouble() - 0.5) * 50) + 75;
        }


        public override double calcSpeed() {return Size * 0.06;}

        public override void Bite(Unit entity)
        {
            if (entity.Size < this.Size * 0.7 && edible.Contains(entity.foodType))
            {
                float multiplier;
                switch(entity.foodType){
                    case FoodType.Meat: 
                        multiplier = 0.2f;
                        break;
                    case FoodType.Plant:
                        multiplier = 0.1f;
                        break;
                    default:
                        multiplier = 0.05f;
                        break;
                }
                Grow(multiplier * entity.Size);
                entity.Die((float)Size, Vector2.Zero);
            }
        }


        public override void Die(float intensity, Vector2 blast)
        {
            //AI Manager removen?
            base.Die(intensity, blast);
        }
    }

    public enum PiranhaFishAIMode
    {
        None,
        Hunter,
        Survival,
        Fuzzy,
    }
}
