﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public static class Options
    {
        public static SpecularFishAIMode SfishAI = SpecularFishAIMode.Coward;
        public static PiranhaFishAIMode PfishAI = PiranhaFishAIMode.Survival;
        public static bool showDebugCounter = false;
        public static bool showHitboxes = false;
    }
}
