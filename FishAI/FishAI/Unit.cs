﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public abstract class Unit
    {
        public OffsetPosition Location;
        public double Size;                 //Actual Size used for eating and stuff, scaling for texture width
        public TextureType Image;

        public float friction;              //Must be below zero, unless you want warpspeed-fish. No you don't.
        public bool Mirrored;               //=true if Fish faces right
        public FoodType foodType;

        public virtual void DoTick(GameTime gameTime) {
            if ((Board.CenterPosition-Location.Vector2).Length() > 6500)
            {
                Die(0f,Vector2.Zero);
                Board.debugCounter++;
            }
        }
        //Texture Center point
        public float TextureOffsetX;
        public float TextureOffsetY;

        public Hitbox CollisionHB;
        public float FactorCollisionHB;

        public Vector2 Momentum;

        public virtual void PassiveCollision(Unit entity) {
            //Momentum += Vector2.Multiply((Location.Vector2-entity.Location.Vector2),0.005f);
            Vector2 force = (Momentum-entity.Momentum);
            Vector2 backwardsforce = (entity.Momentum - Momentum);

            Momentum -= force * (float)((entity.Size/Size) * 0.7f);
            entity.Momentum += force * (float)((Size / entity.Size) * 0.7f);

            Momentum += backwardsforce * (float)(entity.Size / Size) * 0.7f;
            entity.Momentum -= backwardsforce * (float)(Size / entity.Size) * 0.7f;
        }

        public virtual void ActiveCollision(Unit entity) {
            /*Vector2 force = (Momentum - entity.Momentum);
            Momentum -= force * (float)((entity.Size / Size) * 0.7f);
            entity.Momentum += force * (float)((Size / entity.Size) * 0.7f);*/
        }

        protected Unit(Vector2 v) {
            Location = new OffsetPosition(v.X,v.Y);
        }

        public virtual void Die(float intensity, Vector2 blast)
        {
            //Blast for blood splash direction
            //intensity for ammount of bloosplash (determines if it gets devoured)
            Board.UnitReset(this);  //"kills" the entity
        }
    }

    public enum FoodType { 
        Plant,
        Meat,
        Toxic,
    }
}
