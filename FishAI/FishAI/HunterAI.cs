﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class HunterAI:AIManager
    {
        public static readonly float CHANGETARGEDTHREASHOLD = 300f;

        public HunterAI(PiranhaFish piranhaFish)
        {
            Puppet = piranhaFish;
            Puppet.OnFinishedEating += OnEat;
        }

        private void OnEat(object sender, EventArgs e)
        {
            FindSuitableTarget();
        }

        public Unit Target;

        private float lastDistance=0f;
        public override void DoAITick()
        {
            if (Target == null) {
                FindSuitableTarget();
            }
            else {
                float curDistance = (Target.Location.Vector2 - Puppet.Location.Vector2).Length();
                if (curDistance > lastDistance + CHANGETARGEDTHREASHOLD)
                {
                    FindSuitableTarget();
                }
                if (Target != null) { 
                    PlotCourse(Target.Location.Vector2,ApproachMode.predator);
                }
                if(lastDistance > curDistance){
                    lastDistance = curDistance;
                }
            }
            base.DoAITick();
        }

        public void FindSuitableTarget() {
            Unit nearestTarget=null;
            foreach (Unit entity in Board.Unitlist) {
                if (entity is Fish && entity.Size < Puppet.Size * 0.7) {
                    if (nearestTarget==null||(entity.Location.Vector2 - Puppet.Location.Vector2).Length() < (nearestTarget.Location.Vector2 - Puppet.Location.Vector2).Length())
                    nearestTarget = entity;
                }
            }
            Target = nearestTarget;
        }
    }
}
