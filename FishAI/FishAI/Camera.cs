﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace FishAI
{
    public class Camera
    {
        public OffsetPosition Location = new OffsetPosition();
        public double Zoom = 1;

        public void Move(double x, double y)
        {
            Location.X += x;
            Location.Y += y;
        }
    }

    public struct OffsetPosition
    {
        public double X;
        public double Y;
        public Vector2 RoundedPosition { get { return new Vector2((int)X, (int)Y); } }

        public OffsetPosition(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public double OffsetX { get { return X - (int)X; } }
        public double OffsetY { get { return Y - (int)Y; } }

        public Vector2 Vector2 { get { return new Vector2((float)X,(float)Y); } }

        public override String ToString()
        {
            return ("X: " + X + " Y: " + Y);
        }
    }
}
