﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public abstract class AIManager : ControlManager
    {

        public AIManager()
        {

        }

        public AIManager(Fish puppet)
            : this()
        {
            this.Puppet = puppet;
        }

        public virtual void DoAITick()
        {

        }




        public virtual void PlotCourse(Vector2 destination)
        {
            //prepare version with args, like HuntingMode etc.
            Puppet.DestinationPoint = destination;
            Puppet.Swimming = true;
            Vector2 Difference = Puppet.DestinationPoint-Puppet.Location.Vector2;
            Difference.Normalize();
            if (Difference.X<0.2||Difference.X>-0.2)
            {
                if (Difference.X / Difference.Y > 0.25 || Difference.X / Difference.Y < -0.25) { 
                    if (Puppet.DestinationPoint.X > Puppet.Location.X)
                    {
                        Puppet.Mirrored = true;
                    }
                    else
                    {
                        Puppet.Mirrored = false;

                    }
                }
            }
            else {
                ;
            }
        }

        public virtual void PlotCourse(Vector2 destination, ApproachMode mode)
        {
            switch (mode)
            {
                case ApproachMode.predator:
                    PlotCourse(destination - (Puppet.BiteHB.OffsetVector * (float)Puppet.Size));
                    break;
                default:
                    PlotCourse(destination);
                    break;
            }
        }
    }

    public enum ApproachMode
    {
        direct,
        predator,
    }
}
