﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class HungryVeganAI:AIManager
    {
        public static readonly float CHANGETARGEDTHREASHOLD = 300f;

        public HungryVeganAI(SpecularFish specularFish)
        {
            Puppet = specularFish;
            Puppet.OnFinishedEating += OnEat;
        }

        private void OnEat(object sender, EventArgs e)
        {
            FindSuitableTarget();
        }

        public Unit Target;

        private float lastDistance=0f;
        public override void DoAITick()
        {
            if (Target == null) {
                FindSuitableTarget();
            }
            else {
                float curDistance = (Target.Location.Vector2 - Puppet.Location.Vector2).Length();
                if (lastDistance > CHANGETARGEDTHREASHOLD)
                {
                    if (curDistance > lastDistance) {
                        FindSuitableTarget();
                    }
                }
                PlotCourse(Target.Location.Vector2,ApproachMode.predator);
                
                if(lastDistance > curDistance){
                    lastDistance = curDistance;
                }
            }
            base.DoAITick();
        }

        public void FindSuitableTarget() {
            Unit nearestTarget=null;
            foreach (Unit entity in Board.Unitlist) {
                if (entity is Planty && entity.Size < Puppet.Size * 0.7) {
                    if (nearestTarget==null||(entity.Location.Vector2 - Puppet.Location.Vector2).Length() < (nearestTarget.Location.Vector2 - Puppet.Location.Vector2).Length())
                    nearestTarget = entity;
                }
            }
            Target = nearestTarget;
        }
        
    }
}
