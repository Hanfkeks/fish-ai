﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class FuzzyAI:AIManager
    {
        public static readonly int AITICKDELAY = 600;    //Get ticked every n-th frame
        public static readonly float VISIONRANGE = 3000f;

        public FuzzyAI(Fish fish)
        {
            Puppet = fish;
            Puppet.OnFinishedEating += OnEat;
            randomLocation = Board.GenerateVectorArround(Puppet.Location.Vector2, 100f, 700f);
        }

        private void OnEat(object sender, EventArgs e)
        {
            FindSuitableTarget();
            aitick = 0;
        }

        public override void DoAITick()
        {
           if (aitick >= AITICKDELAY)
           {
               FindSuitableTarget();
               aitick = 0;
           }
           aitick++;
           if (Target == null)
           {
               if ((randomLocation - Puppet.Location.Vector2).Length() < 20f)
               {
                   randomLocation = Board.GenerateVectorArround(Puppet.Location.Vector2, 100f, 700f);
               }
               PlotCourse(randomLocation, ApproachMode.direct);
           }
           else
           {
               switch (Status)
               {
                   case StateAIStatus.Eat:
                       PlotCourse(Target.Location.Vector2, ApproachMode.predator);
                       break;
                   case StateAIStatus.Flee:
                       PlotCourse(Puppet.Location.Vector2 + (Puppet.Location.Vector2 - Target.Location.Vector2) * 2f, ApproachMode.direct);
                       break;
                   default:
                       //not supposed to happen...
                       break;
               }
           }
            base.DoAITick();
        }

        public int aitick = (int)Board.Maprand.NextDouble() * AITICKDELAY;
        public Vector2 randomLocation;
        public Unit Target;
        public Fuzzy TargetRating = new Fuzzy(0f);
        public StateAIStatus Status = StateAIStatus.Idle;

        public void FindSuitableTarget() {
            LinkedList<FuzzyPair> relevantUnits = new LinkedList<FuzzyPair>();
            Unit fastest=null;
            Unit furthest=null;
            int mostCompetitive=0;
            foreach(Unit entity in Board.Unitlist){
                if (entity != Puppet) {
                    FuzzyPair pair = new FuzzyPair(entity);
                    relevantUnits.AddFirst(pair);
                    if (fastest == null || fastest.Momentum.Length() < entity.Momentum.Length()) {
                        fastest = entity;
                    }
                    if (furthest == null || (furthest.Location.Vector2-Puppet.Location.Vector2).Length() < (entity.Location.Vector2 - Puppet.Location.Vector2).Length()) {
                        furthest = entity;
                    }
                    /*int targeted = 0;
                    foreach(AIManager ai in Board.AIList){
                        //cubic complexity (!!!)
                        if (ai is FuzzyAI && ((FuzzyAI)ai).Target==Puppet && ai.Puppet.edible.Contains(Puppet.foodType))
                        {
                            targeted++;
                        }
                    }
                    if (targeted > mostCompetitive) {
                        mostCompetitive = targeted;
                    }
                    pair.targeted = targeted;*/
                }
            }
            FuzzyPair highestFoodRating=null;
            FuzzyPair highestDangerRating=null;
            foreach(FuzzyPair pair in relevantUnits){
                pair.Fuzzyfication(Puppet,furthest,mostCompetitive,fastest);
                if (highestFoodRating == null || pair.FoodRating.Value > highestFoodRating.FoodRating.Value) {
                    highestFoodRating = pair;
                }
                if (highestDangerRating == null || pair.DangerRating.Value > highestDangerRating.DangerRating.Value)
                {
                    highestDangerRating = pair;
                }
            }
            if (highestFoodRating.FoodRating.Value < highestDangerRating.DangerRating.Value)
            {
                Target = highestDangerRating.Targetunit;
                Status = StateAIStatus.Flee;
            }
            else {
                Target = highestFoodRating.Targetunit;
                Status = StateAIStatus.Eat;
            }
        }
        
    }
}
