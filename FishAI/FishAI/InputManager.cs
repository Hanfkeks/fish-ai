﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FishAI
{
    public class InputManager : ControlManager
    {

        public InputManager()
        {
            Board.MouseClicked += Click;
        }

        public InputManager(Fish puppet)
            : this()
        {
            this.Puppet = puppet;
        }

        private void Click(MouseButton button, MouseState state, Vector2 absoluteCoords)
        {
            if (button == MouseButton.LeftMB)
            {
                this.Puppet.DestinationPoint = absoluteCoords;
                this.Puppet.Swimming = true;

                Puppet.Mirrored = (absoluteCoords.X > Puppet.Location.X);


            }
        }

    }
}
