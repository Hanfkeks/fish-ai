﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class TridentFish : Fish
    {
        public TridentFish(Vector2 v)
            : base(v)
        {
            TextureOffsetX = 0.4f;
            TextureOffsetY = 0.5f;
            Size = ((Board.Maprand.NextDouble() - 0.5) * 25) + 70;
            this.Image = TextureType.TridentFish;
            friction = 0.0015f;
            FactorCollisionHB = 0.6f;
            FactorBiteHB = 0.2f;
            CollisionHB = new Hitbox(this, new Vector2(0f, 0f), (float)(Size * FactorCollisionHB), false);
            BiteHB = new Hitbox(this, new Vector2(0.6f, 0f), (float)(Size * FactorBiteHB), true);
            edible.Add(FoodType.Meat);
            edible.Add(FoodType.Toxic);
            edible.Add(FoodType.Plant);
        }

        public Camera focusedCamera;

        public override void DoTick(GameTime gameTime) {
            if (focusedCamera != null) {
                focusedCamera.Location = Location;
            }
            base.DoTick(gameTime);
        }

        public override double calcSpeed() { return Size * 0.1; }

        public override void Bite(Unit entity)
        {
            if (entity.Size < this.Size * 0.7 && edible.Contains(entity.foodType))
            {
                if (entity.foodType == FoodType.Meat)
                {
                    Grow(0.15 * entity.Size);
                    entity.Die((float)Size, Vector2.Zero);
                }
                else if (entity.foodType == FoodType.Plant)
                {
                    Grow(0.10 * entity.Size);
                    entity.Die((float)Size, Vector2.Zero);
                }
            }
            else {
                this.ActiveCollision(entity);
                entity.PassiveCollision(this);
            }
        }


    }
}
