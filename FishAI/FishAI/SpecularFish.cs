﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class SpecularFish:Fish
    {
        public SpecularFish(Vector2 v)
            : base(v)
        {
            TextureOffsetX = 0.4f;
            TextureOffsetY = 0.5f;
            Size = ((Board.Maprand.NextDouble() - 0.5) * 50) + 50;
            this.Image = TextureType.SpecularFish;
            friction = 0.002f;
            FactorCollisionHB = 0.6f;
            FactorBiteHB = 0.2f;
            CollisionHB = new Hitbox(this, new Vector2(0f, 0f), (float)(Size * FactorCollisionHB), false);
            BiteHB = new Hitbox(this, new Vector2(0.7f, 0.0f), (float)(Size * FactorBiteHB), true);
            edible.Add(FoodType.Plant);
            //AI Test
            switch (Options.SfishAI)
            {
                case SpecularFishAIMode.HungryVegan:
                    FishCM = new HungryVeganAI(this);
                    Board.AIList.AddFirst((AIManager)FishCM);
                    break;
                case SpecularFishAIMode.Coward:
                    FishCM = new CowardAI(this);
                    Board.AIList.AddFirst((AIManager)FishCM);
                    break;
                case SpecularFishAIMode.Fuzzy:
                    FishCM = new FuzzyAI(this);
                    Board.AIList.AddFirst((AIManager)FishCM);
                    break;
                default:
                    break;
            }
        }


        public override double calcSpeed() {return Size * 0.1;}

        public override void Bite(Unit entity)
        {
            if (entity.Size < this.Size * 0.7 && edible.Contains(entity.foodType))
            {
                float multiplier;
                switch(entity.foodType){
                    case FoodType.Meat: 
                        multiplier = 0.2f;
                        break;
                    case FoodType.Plant:
                        multiplier = 0.1f;
                        break;
                    default:
                        multiplier = 0.05f;
                        break;
                }
                Grow(multiplier * entity.Size);
                entity.Die((float)Size, Vector2.Zero);
            }
        }


        public override void Die(float intensity, Vector2 blast)
        {
            //AI Manager removen?
            base.Die(intensity, blast);
        }

        
    }

    public enum SpecularFishAIMode { 
        None,
        HungryVegan,
        Coward,
        Fuzzy,
    }
}
