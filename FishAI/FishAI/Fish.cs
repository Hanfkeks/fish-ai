﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FishAI
{
    public abstract class Fish : Unit
    {
        public static readonly float SWIMMINGTHREASHOLD = 5.0f;

        protected Fish(Vector2 v)
            : base(v)
        {
            DestinationPoint = Location.Vector2;
            Swimming = false;
            Mirrored = false;
            foodType = FoodType.Meat;
        }

        public List<FoodType> edible = new List<FoodType>();

        public abstract double calcSpeed();

        public ControlManager FishCM;
        public Vector2 DestinationPoint;

        public bool Swimming;



        public override void DoTick(GameTime gameTime)
        {
            //apply speed to momentum
            if(Swimming){
                Vector2 direction = DestinationPoint -Location.Vector2;
                if (direction.Length() > SWIMMINGTHREASHOLD) {
                    direction.Normalize();
                    Momentum.X += direction.X * (float)(gameTime.ElapsedGameTime.Milliseconds * calcSpeed()/1000);
                    Momentum.Y += direction.Y * (float)(gameTime.ElapsedGameTime.Milliseconds * calcSpeed()/1000);
                }
                Swimming = false;
                //Send end swimming event
            }
            
            //calc friction
            Momentum -= new Vector2((gameTime.ElapsedGameTime.Milliseconds * friction * Momentum.X),(gameTime.ElapsedGameTime.Milliseconds * friction * Momentum.Y));
            //apply momentum and friction to position
            Location.X+=Momentum.X;
            Location.Y += Momentum.Y;
            //scan HB of new location
            foreach (Unit entity in Board.Unitlist) { 
                if(BiteHB.CollisionCheck(entity))
                {
                    Bite(entity);
                }
                else if (CollisionHB.CollisionCheck(entity))
                {
                    ActiveCollision(entity);
                    entity.PassiveCollision(this);
                }
            }
            base.DoTick(gameTime);

        }

        public virtual void Grow(double growth){
            Size+=growth;
            float percentual = (float)(growth / Size);
            //BiteHB.OffsetVector = new Vector2(percentual + BiteHB.getUnchangedOffset().X, percentual + BiteHB.getUnchangedOffset().Y);
            BiteHB.Radius = (float)Size * FactorBiteHB;
            //CollisionHB.OffsetVector = new Vector2(percentual + CollisionHB.getUnchangedOffset().X, percentual + CollisionHB.getUnchangedOffset().Y);
            CollisionHB.Radius = (float)Size * FactorCollisionHB;
            RaiseFinishedEating();
        }

        /*public delegate void FinishedEatingEvent();
        public event FinishedEatingEvent FinishedEating;
        public void RaiseFinishedEating()
        {
            FinishedEating();
        }*/
        public delegate void FinishedEating(object sender, EventArgs e);
        public event FinishedEating OnFinishedEating;
        public void RaiseFinishedEating() {
            if (OnFinishedEating != null) { 
                OnFinishedEating(this,EventArgs.Empty);
            }
        }

        public Hitbox BiteHB;
        public float FactorBiteHB;

        public abstract void Bite(Unit entity);
    }
}
