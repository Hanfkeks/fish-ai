﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class Planty:Unit
    {
         public Planty(Vector2 v)
            : base(v)
        {
            TextureOffsetX = 0.5f;
            TextureOffsetY = 0.5f;
            Size = ((Board.Maprand.NextDouble() - 0.5) * 15) + 22;
            this.Image = TextureType.Planty;
            friction = 0.0006f;
            FactorCollisionHB = 0.6f;
            CollisionHB = new Hitbox(this, new Vector2(0f, 0f), (float)(Size * FactorCollisionHB), false);
            foodType = FoodType.Plant;
        }

         public override void DoTick(GameTime gameTime)
         {
             Momentum -= new Vector2((gameTime.ElapsedGameTime.Milliseconds * friction * Momentum.X), (gameTime.ElapsedGameTime.Milliseconds * friction * Momentum.Y));
             //apply momentum and friction to position
             Location.X += Momentum.X;
             Location.Y += Momentum.Y;
             //scan HB of new location
             foreach (Unit entity in Board.Unitlist)
             {
                 if (CollisionHB.CollisionCheck(entity))
                 {
                     ActiveCollision(entity);
                     entity.PassiveCollision(this);
                 }
             }
             base.DoTick(gameTime);
         }
        //Rotation based on Momentum?
    }
}
