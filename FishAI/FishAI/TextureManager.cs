﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace FishAI
{
    public static class TextureManager
    {
        private static double currentZoom = 0;

        public static Texture2D[] Textures;
        //private static Texture2D[] BufferedScaledTextures;     //Private because you should access it via getBufferedFloor()

        public static void startNewTick(double zoom)
        {
            if (zoom == currentZoom)
            {
                return;
            }
            else
            {
                currentZoom = zoom;
                //BufferedScaledTextures = new Texture2D[Enum.GetValues(typeof(TextureType)).Length];
            }
        }

        public static Texture2D getTexture(TextureType type) {
            return Textures[(int)type];
        }

        /*public static Texture2D getBufferedFloor(TextureType type)
        {
            int index = (int)type;
            if (BufferedScaledTextures[index] == null)
            {
                Texture2D original = Textures[index];
                BufferedScaledTextures[index] = new Texture2D(original, original.Width*currentZoom, original.Height*currentZoom);
            }
            return BufferedScaledTextures[index];
        }*/     //Scaling is not a problem in XNA, Fish is probably being scaled all the time in the final version, so screw this

        public static void loadTexture(Texture2D texture, TextureType type)
        {
            Textures[(int)type] = texture;
        }

        public static void prepareLoading() {
            Textures = new Texture2D[Enum.GetValues(typeof(TextureType)).Length];
        }
    }

    public enum TextureType
    {
        SpecularFish,
        TridentFish,
        DebuggingCircle,
        Planty,
        PiranhaFish,
        Background,
    }
}
