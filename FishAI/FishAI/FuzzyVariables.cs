﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public struct Fuzzy
    {

        public Fuzzy(float value) {
            this.Value = value;
        }

        public float Value;

        public static Fuzzy AND(Fuzzy one, Fuzzy two){
            if (one.Value > two.Value)
            {
                return two;
            }
            else return one;
        }

        public static Fuzzy OR(Fuzzy one, Fuzzy two)
        {
            if (one.Value < two.Value)
            {
                return two;
            }
            else return one;
        }

        public float NOT() {
            return 1f - this.Value;
        }

        public static Fuzzy AND(Fuzzy[] farray) {
            Fuzzy min=new Fuzzy(float.PositiveInfinity);
            foreach (Fuzzy f in farray) {
                if (f.Value < min.Value) {
                    min = f;
                }
            }
            return min;
        }

        public static Fuzzy OR(Fuzzy[] farray)
        {
            Fuzzy max = new Fuzzy(0);
            foreach (Fuzzy f in farray)
            {
                if (f.Value > max.Value)
                {
                    max = f;
                }
            }
            return max;
        }

    }

    public class FuzzyPair{
        
        public FuzzyPair(Unit target){
            Targetunit = target;
        }
        public int targeted = 0;
        //Membership Values for Agression/Defense
        public Fuzzy Attackable = new Fuzzy(0f);
        public Fuzzy Dangerous = new Fuzzy(0f);
        public Fuzzy Obstacle = new Fuzzy(1f);

        //Membership Values for Size
        public Fuzzy Valuable = new Fuzzy(0f);
        public Fuzzy Useless = new Fuzzy(1f);

        //Membership Values for Competition
        public Fuzzy HardCompetition = new Fuzzy(0f);
        public Fuzzy EasyCompetition = new Fuzzy(1f);

        //Membership Values for Distance
        public Fuzzy Near = new Fuzzy(0f);
        public Fuzzy Far = new Fuzzy(1f);

        //Membership Values for Momentum
        public Fuzzy Incomming = new Fuzzy(0f);
        public Fuzzy Escaping = new Fuzzy(1f);

        public Unit Targetunit;

        //Black Magic Incomming:
        public void Fuzzyfication(Fish originFish, Unit furthest, int mostTargets, Unit fastest) { 
            //furthest defines what 1.0f of far is
            
            Far.Value = (originFish.Location.Vector2-Targetunit.Location.Vector2).Length()/(originFish.Location.Vector2-furthest.Location.Vector2).Length();
            Near.Value = Far.NOT();

            //mostTargets is the highest ammount of fish that target the same unit and defines what 1.0 of HardCompetition is
            if (mostTargets < targeted)
            {
                HardCompetition.Value = 1f;
            }
            else if(mostTargets > 0){
                HardCompetition.Value = targeted / mostTargets;
            }    
            else HardCompetition.Value = 0f;
            EasyCompetition.Value = HardCompetition.NOT();

            //fastest is used to determine Incomming and Escaping
            //Explaining this is too hard
            float angle = (float)Math.Acos(Vector2.Dot(Targetunit.Location.Vector2-originFish.Location.Vector2,Targetunit.Momentum));
            Console.Out.WriteLine(angle+"° Angle");
            float anglemodifier = (Math.Abs(angle)/180f)-0.5f;
            Console.Out.WriteLine(anglemodifier + " Anglemodifier");
            Escaping.Value = 0.5f + (anglemodifier * (Targetunit.Momentum - originFish.Momentum).Length() / (fastest.Momentum - originFish.Momentum).Length());
            Console.Out.WriteLine(Escaping.Value + " Escaping Value");
            Incomming.Value = Escaping.NOT();

            //Calculate Agression
            if (originFish.Size < Targetunit.Size * 1.3) {
                if (Targetunit is Fish && ((Fish)Targetunit).edible.Contains(originFish.foodType))
                {
                    Attackable.Value = 0.0f;
                    Obstacle.Value = 0.0f;
                    Dangerous.Value = 1.0f;
                }
                else {
                    Attackable.Value = 0.0f;
                    Obstacle.Value = 1.0f;
                    Dangerous.Value = 0.0f;
                }
            }
            else if (originFish.Size < Targetunit.Size * 1.3 && originFish.Size > Targetunit.Size * 0.9) {

                if (Targetunit is Fish && ((Fish)Targetunit).edible.Contains(originFish.foodType))
                {
                    float fuzzyval = (float)(originFish.Size - (Targetunit.Size * 0.9)) / (float)(Targetunit.Size * 0.4);
                    Dangerous.Value = fuzzyval;
                    Obstacle.Value = Dangerous.NOT();
                    Attackable.Value = 0f;
                }
                else {
                    Attackable.Value = 0.0f;
                    Obstacle.Value = 1.0f;
                    Dangerous.Value = 0.0f;
                }
            }
            else if (originFish.Size < Targetunit.Size * 0.9 && originFish.Size > Targetunit.Size * 0.75)
            {
                Attackable.Value = 0.0f;
                Obstacle.Value = 1.0f;
                Dangerous.Value = 0.0f;
            }
            else if (originFish.Size < Targetunit.Size * 0.75 && originFish.Size > Targetunit.Size * 0.60)
            {
                if (originFish.edible.Contains(Targetunit.foodType))
                {
                    float fuzzyval = (float)(originFish.Size - (Targetunit.Size * 0.60)) / (float)(Targetunit.Size * 0.15);
                    Dangerous.Value = 0f;
                    Obstacle.Value = fuzzyval;
                    Attackable.Value = Obstacle.NOT();
                }
                else
                {
                    Attackable.Value = 0.0f;
                    Obstacle.Value = 1.0f;
                    Dangerous.Value = 0.0f;
                }
            }
            else if (originFish.Size < Targetunit.Size * 0.60) {
                if (originFish.edible.Contains(Targetunit.foodType))
                {
                    Attackable.Value = 1.0f;
                    Obstacle.Value = 0.0f;
                    Dangerous.Value = 0.0f;
                }
                else
                {
                    Attackable.Value = 0.0f;
                    Obstacle.Value = 1.0f;
                    Dangerous.Value = 0.0f;
                }
            }


            //Calculate Valuability (optional)

        }

        public Fuzzy DangerRating { get { return new Fuzzy(Dangerous.Value*Incomming.Value*Near.Value);} }
        public Fuzzy FoodRating { get { return new Fuzzy(Attackable.Value * Incomming.Value*Near.Value); } }
        
    }
}
