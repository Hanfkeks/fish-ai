﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public static class Board
    {
        public static LinkedList<Unit> Unitlist;
        public static LinkedList<AIManager> AIList;
        public static Camera CenteredCam;
        public static int debugCounter = 0;
        public static Vector2 CenterPosition{
            get{return CenteredCam.Location.Vector2;}
        }      //Despawn and Respawn objects near this

        public static Random Maprand = new Random();

        public static void PrepareBoard() {
            Unitlist = new LinkedList<Unit>();
            AIList = new LinkedList<AIManager>();
        }

        public static void GenerateMap(Camera initialCam) {
            CenteredCam = initialCam;
            for (int i = 0; i < 11; i++)
            {
                Unitlist.AddFirst(new PiranhaFish(GenerateVectorArround(initialCam.Location.Vector2, 300, 2000)));
            }
            for (int i = 0; i < 25; i++) {
                Unitlist.AddFirst(new SpecularFish(GenerateVectorArround(initialCam.Location.Vector2, 300, 2000)));
            }
            for (int i = 0; i < 35; i++)
            {
                Unitlist.AddFirst(new Planty(GenerateVectorArround(initialCam.Location.Vector2, 300, 2000)));
            }
            TridentFish controlfish = new TridentFish(new Vector2(0, 0));
            InputManager inputManager = new InputManager(controlfish);
            Unitlist.AddFirst(controlfish);
            controlfish.focusedCamera = initialCam;
            controlfish.Mirrored = true;
        }

        public static void UnitReset(Unit entity) { 
            //Vector2 newpos = GenerateVectorArround(CenterPosition,0, 0);
            Vector2 newpos = GenerateVectorArround(CenteredCam.Location.Vector2, 2400, 3700);
            entity.Location.X = newpos.X;
            entity.Location.Y = newpos.Y;
            entity.Momentum = Vector2.Zero;
            if (entity is SpecularFish) {
                SpecularFish spec = entity as SpecularFish;
                spec.Size = ((Maprand.NextDouble() - 0.5) * 50) + 50;
                spec.CollisionHB = new Hitbox(spec, new Vector2(spec.TextureOffsetX, spec.TextureOffsetY), (float)(spec.Size * 0.6), false);
                spec.BiteHB = new Hitbox(spec, new Vector2(0.7f, 0.5f), (float)(spec.Size * 0.5), true);
            }
            else if (entity is PiranhaFish) {
                PiranhaFish pir = entity as PiranhaFish;
                pir.Spawn();
            }
        }

        public static Vector2 GenerateVectorArround(Vector2 center, float minradius, float maxradius) { 
           float randomLength = (float)(Maprand.NextDouble()*(maxradius-minradius))+minradius;
           Vector2 randVector = new Vector2(randomLength,0);
           float randomRotation = (float)(Maprand.NextDouble()*360);
           randVector = Vector2.Transform(randVector, Matrix.CreateRotationZ(randomRotation));
           return center+randVector;
        }

        
        public delegate void MouseClickedEvent(MouseButton button, MouseState state, Vector2 absoluteCoords);
        public static event MouseClickedEvent MouseClicked;
        public static void RaiseMouseClicked(MouseButton args, MouseState state, Vector2 absoluteCoords)
        {
            Board.MouseClicked(args,state,absoluteCoords);
        }

    }

    public enum MouseButton
    {
        LeftMB,
        RightMB,
    }

}
