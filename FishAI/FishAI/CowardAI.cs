﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishAI
{
    public class CowardAI:AIManager
    {
        public static readonly float CHANGETARGEDTHREASHOLD = 300f;
        public static readonly float VISIONRANGE = 3000f;

        public CowardAI(SpecularFish specularFish)
        {
            Puppet = specularFish;
            Puppet.OnFinishedEating += OnEat;
            randomLocation = Board.GenerateVectorArround(Puppet.Location.Vector2, 100f, 700f);
        }

        private void OnEat(object sender, EventArgs e)
        {
            FindSuitableTarget();
        }

        public Vector2 randomLocation;
        public Unit Target;
        public StateAIStatus Status = StateAIStatus.Idle;

        private float lastDistance=0f;
        public override void DoAITick()
        {
            FindSuitableTarget();
            if (Target == null) {
                if (Status==StateAIStatus.Idle&&(randomLocation - Puppet.Location.Vector2).Length() < 20f) {
                    randomLocation = Board.GenerateVectorArround(Puppet.Location.Vector2, 400f, 800f);
                }
                PlotCourse(randomLocation, ApproachMode.direct);
            }
            else {
                float curDistance = (Target.Location.Vector2 - Puppet.Location.Vector2).Length();
                if (lastDistance > +CHANGETARGEDTHREASHOLD)
                {
                    if (curDistance > lastDistance) {
                        FindSuitableTarget();
                    }
                }
                switch (Status) {
                    case StateAIStatus.Eat:
                        PlotCourse(Target.Location.Vector2,ApproachMode.predator);
                        break;
                    case StateAIStatus.Flee:
                        PlotCourse(Puppet.Location.Vector2+(Puppet.Location.Vector2-Target.Location.Vector2)*2f, ApproachMode.direct);
                        break;
                }
                if(lastDistance > curDistance){
                    lastDistance = curDistance;
                }
            }
            base.DoAITick();
        }

        public void FindSuitableTarget() {
            Unit nearestTarget=null;
            foreach (Unit entity in Board.Unitlist) {
                if (entity is Planty && entity.Size < Puppet.Size * 0.7) {
                    if (nearestTarget==null||(entity.Location.Vector2 - Puppet.Location.Vector2).Length() < (nearestTarget.Location.Vector2 - Puppet.Location.Vector2).Length()){
                        nearestTarget = entity;
                        Status = StateAIStatus.Eat;
                    }
                }
                else if (entity is Fish && entity.Size * 0.7 > Puppet.Size && ((Fish)entity).edible.Contains(Puppet.foodType)) {
                    if (nearestTarget == null || (entity.Location.Vector2 - Puppet.Location.Vector2).Length() < (nearestTarget.Location.Vector2 - Puppet.Location.Vector2).Length()) {
                        nearestTarget = entity;
                        Status = StateAIStatus.Flee;
                    }
                }
            }
            if ((nearestTarget.Location.Vector2 - Puppet.Location.Vector2).Length() > VISIONRANGE)
            {
                Target = null;
                Status = StateAIStatus.Idle;
            }
            Target = nearestTarget;
        }
    }

    public enum StateAIStatus { 
        Idle,
        Flee,
        Eat,
    }
}
